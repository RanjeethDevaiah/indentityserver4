﻿using IdentityServer4.AspNetIdentity;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;

namespace IdentityServer.Services
{
    public class UserAccountProfileService : ProfileService<ApplicationUser>
    {
        public UserAccountProfileService(UserManager<ApplicationUser> userManager, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory) : base(userManager, claimsFactory)
        {
        }
    }
}
