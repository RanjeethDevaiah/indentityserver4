﻿using Microsoft.AspNetCore.Identity;
using IdentityServer.Models;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Security.Claims;
using IdentityModel;

public sealed class ClaimsPrincipalFactory : UserClaimsPrincipalFactory<ApplicationUser, IdentityRole>
{
    public ClaimsPrincipalFactory(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IOptions<IdentityOptions> optionsAccessor)
            : base(userManager, roleManager, optionsAccessor)
    {}

    protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
    {
        var identity = await base.GenerateClaimsAsync(user);

        if (!identity.HasClaim(x => x.Type == JwtClaimTypes.Subject))
        {
            // var sub = user.;
            //identity.AddClaim(new Claim(JwtClaimTypes.Subject, sub));
        }

        return identity;
    }
}